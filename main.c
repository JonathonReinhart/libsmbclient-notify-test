#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <samba-4.0/libsmbclient.h>

#define fail_errno(fmt, ...)    error(2, errno, fmt, ##__VA_ARGS__)
#define debug(fmt, ...)         fprintf(stderr, fmt, ##__VA_ARGS__)

#define DEBUG_LEVEL     12

#define NOTIFY_ALL  (\
    SMBC_NOTIFY_CHANGE_FILE_NAME    | \
    SMBC_NOTIFY_CHANGE_DIR_NAME     | \
    SMBC_NOTIFY_CHANGE_ATTRIBUTES   | \
    SMBC_NOTIFY_CHANGE_SIZE         | \
    SMBC_NOTIFY_CHANGE_LAST_WRITE   | \
    SMBC_NOTIFY_CHANGE_LAST_ACCESS  | \
    SMBC_NOTIFY_CHANGE_CREATION     | \
    SMBC_NOTIFY_CHANGE_EA           | \
    SMBC_NOTIFY_CHANGE_SECURITY     | \
    SMBC_NOTIFY_CHANGE_STREAM_NAME  | \
    SMBC_NOTIFY_CHANGE_STREAM_SIZE  | \
    SMBC_NOTIFY_CHANGE_STREAM_WRITE )


static const char *action_name(int a)
{
    static char buf[80];
    switch (a) {
        case SMBC_NOTIFY_ACTION_ADDED:              return "added";
        case SMBC_NOTIFY_ACTION_REMOVED:            return "removed";
        case SMBC_NOTIFY_ACTION_MODIFIED:           return "modified";
        case SMBC_NOTIFY_ACTION_OLD_NAME:           return "old name";
        case SMBC_NOTIFY_ACTION_NEW_NAME:           return "new name";
        case SMBC_NOTIFY_ACTION_ADDED_STREAM:       return "added stream";
        case SMBC_NOTIFY_ACTION_REMOVED_STREAM:     return "removed stream";
        case SMBC_NOTIFY_ACTION_MODIFIED_STREAM:    return "modified stream";
        default:
            snprintf(buf, sizeof(buf), "??? (%d)", a);
            return buf;
    }
}

static int notify_callback(
	const struct smbc_notify_callback_action *actions,
	size_t num_actions, void *private_data)
{
    size_t i;

    printf("Notify callback:\n");
    for (i=0; i<num_actions; i++) {
        const struct smbc_notify_callback_action *act = &actions[i];
        printf("  %s %s\n", action_name(act->action), act->filename);
    }

    return 0;
}

int main(int argc, char **argv)
{
    SMBCCTX *ctx = NULL;
    SMBCFILE *dh = NULL;
    const char *url;
    int rc;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s URL\n", program_invocation_short_name);
        exit(1);
    }
    url = argv[1];


    // Setup context
    ctx = smbc_new_context();
    if (!ctx)
        fail_errno("smbc_new_context() failed");

    if (!smbc_init_context(ctx))
        fail_errno("smbc_init_context() failed");

    smbc_setOptionUseKerberos(ctx, 1);
    smbc_setOptionFallbackAfterKerberos(ctx, 0);
    smbc_setDebug(ctx, DEBUG_LEVEL);


    // Open directory
    dh = smbc_getFunctionOpendir(ctx)(ctx, url);
    if (!dh)
        fail_errno("Failed to open directory");
    debug("Opened directory %s\n", url);


    // Notify
    smbc_bool recursive = 1;
    uint32_t filter = NOTIFY_ALL;
    unsigned timeout = 5000;

    rc = smbc_getFunctionNotify(ctx)(
            ctx, dh, recursive, filter, timeout, notify_callback, NULL);
    if (rc)
        fail_errno("Notify failed (%d)", errno);


    // Close up shop
    smbc_getFunctionClosedir(ctx)(ctx, dh);
    dh = NULL;

    if (smbc_free_context(ctx, 1))
        error(0, errno, "smbc_free_context() failed");
    ctx = NULL;

    return 0;
}
