libsmbclient-notify-test
========================

Compare the behavior of this program:
```
$ scons
$ ./smbnotifytest smb://dc1.example.com/sysvol
```


with:
```
$ smbclient -kN //dc1.example.com/sysvol -c 'notify /'
```
